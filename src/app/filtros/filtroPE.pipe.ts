import { Pipe, PipeTransform } from '@angular/core';
import { InjectFlags } from '@angular/compiler/src/core';

@Pipe({
  name: 'filtroif'
})
export class FiltroPEPipe implements PipeTransform {

  transform(value: any[], fechaini: Date, fechafinal: Date, columna: string): any[] {
    if (!value) return [];
    if (!fechaini) return value;
    if (!fechafinal) return value;
    return value.filter(it => {
      return it[columna] >= fechaini && it[columna] <= fechafinal ? it[columna]  : null;
    });
  }

}
