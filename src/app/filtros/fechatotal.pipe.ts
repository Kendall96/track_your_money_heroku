import { Pipe, PipeTransform } from '@angular/core';

import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';

@Pipe({
  name: 'fechatotal'
})
export class FechatotalPipe implements PipeTransform {

  transform(value: any[], fechaini: NgbDateStruct, fechafinal: NgbDateStruct): any[] {
    if (!value) return [];
    if (!fechaini) return value;
    if (!fechafinal) return value;

    return value.filter(it => {
      return new Date(it.fecha) >= new Date(fechaini.year, fechaini.month, fechaini.day) &&
       new Date(it.fecha) <= new Date(fechafinal.year, fechafinal.month, fechafinal.day) ? it : null;
    }).reduce((a, b) => a + parseInt(b.monto.toString()), 0);
  }
}
