import { Pipe, PipeTransform } from '@angular/core';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Pipe({
  name: 'texto'
})
export class TextoPipe implements PipeTransform {

  transform(value: any, valor: string, inicial: NgbDateStruct, final: NgbDateStruct, ultimomes: Date, ultimoano: Date, mes: number, ano: number): any {
    if (value) {
      return value.filter(it => {
        if ((valor != null || valor != '' || valor != undefined) && inicial != undefined && final != undefined) {
          if (new Date(it.fecha) >= new Date(inicial.year, inicial.month, inicial.day) &&
            new Date(it.fecha) <= new Date(final.year, final.month, final.day) && (it['nombre_transaccion'].toLowerCase().includes(valor.toLowerCase()) || it['nombre_cuenta'].toLowerCase().includes(valor.toLowerCase()))) {
            return true;
          } else {
            return false;
          }
        } else if ((valor != null || valor != '' || valor != undefined) && ultimomes != null) {
          console.log(new Date(it.fecha));
          if (new Date(it.fecha) >= new Date(ultimomes) && new Date(it.fecha) <= new Date() && (it['nombre_transaccion'].toLowerCase().includes(valor.toLowerCase()) || it['nombre_cuenta'].toLowerCase().includes(valor.toLowerCase()))) {
            console.log("llego" + ultimomes);
            return true;
          } else {
            return false;
          }
        }else if((valor != null || valor != '' || valor != undefined) && ultimoano != null){
          if (new Date(it.fecha) >= ultimoano && (it['nombre_transaccion'].toLowerCase().includes(valor.toLowerCase()) || it['nombre_cuenta'].toLowerCase().includes(valor.toLowerCase()))) {
            console.log("llego" + ultimoano);
            return true;
          } else {
            return false;
          }
        }else if((valor != null || valor != '' || valor != undefined) && mes != 0){
          if (new Date(it.fecha).getMonth() == mes && (it['nombre_transaccion'].toLowerCase().includes(valor.toLowerCase()) || it['nombre_cuenta'].toLowerCase().includes(valor.toLowerCase()))) {
            return true;
          } else {
            return false;
          }
        }
        else if((valor != null || valor != '' || valor != undefined) && ano != 0){
          if (new Date(it.fecha).getFullYear() == ano && (it['nombre_transaccion'].toLowerCase().includes(valor.toLowerCase()) || it['nombre_cuenta'].toLowerCase().includes(valor.toLowerCase()))) {
            return true;
          } else {
            return false;
          }
        }
        else {
          return false;
        }

      });

    }
    else {
      return false;
    }
  }

}
