import { Component, OnInit } from '@angular/core';
import { DataService } from './../services/data.service';
import { DefinirMoneda } from '../modelos/definir-moneda';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthServiceToken } from './../shared/auth.service';
import { JWTToken } from './../shared/token';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Ng2IzitoastService } from 'ng2-izitoast';

@Component({
  selector: 'app-definir-moneda',
  templateUrl: './definir-moneda.component.html',
  styleUrls: ['./definir-moneda.component.scss']
})
export class DefinirMonedaComponent implements OnInit {

  money: DefinirMoneda[] = [];
  data_money = new DefinirMoneda;
  datos_moneda = new DefinirMoneda;
  name_money: string;
  id: number;
  simbolo: string;
  descripcion: string;
  tasa: bigint;
  closeResult: string;
  token: JWTToken;

  constructor(private dataService: DataService,public iziToast: Ng2IzitoastService, private authService: AuthServiceToken, private modal: NgbModal, private router: Router) { }

  getTypes_Money(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getMoney(this.token, sessionStorage.getItem('user')).subscribe(data => { 
          this.money = data;
          console.log(this.money);
        });
      });
  }

  addMoney(formulario: NgForm){  
    this.data_money.id = this.id;
    this.data_money.nombre_moneda = this.name_money;
    this.data_money.simbolo = this.simbolo;
    this.data_money.descripcion = this.descripcion;
    this.data_money.tasa = this.tasa;
    this.data_money.email_usuario = sessionStorage.getItem('user');

    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.addMoney(this.token, this.data_money).subscribe(data => { 
          formulario.reset();
          this.getTypes_Money();
          this.modal.dismissAll();
          this.iziToast.success({title: "Exito",
          message: 'Se creó una moneda', position: 'topRight'});
        });
      });
  }

  updateMoney(formulario1: NgForm){  
    this.data_money.id = this.datos_moneda.id;
    this.data_money.nombre_moneda = this.datos_moneda.nombre_moneda;
    this.data_money.simbolo = this.datos_moneda.simbolo;
    this.data_money.descripcion = this.datos_moneda.descripcion;
    this.data_money.tasa = this.datos_moneda.tasa;
    this.data_money.email_usuario = sessionStorage.getItem('user');

    console.log(this.datos_moneda.nombre_moneda);
    
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.updateMoney(this.token, this.data_money).subscribe(data => { 
          formulario1.reset();
          this.getTypes_Money();
          this.modal.dismissAll();
          this.iziToast.success({title: "Exito",
          message: 'Se modificó una moneda', position: 'topRight'});
        });
      });
  }

  deleteMoney(id: number){
    //console.log(id + sessionStorage.getItem('user'));
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.deleteMoneys(this.token, id).subscribe(date => {
          this.getTypes_Money();
          this.iziToast.success({title: "Exito",
          message: 'Se eliminó una moneda', position: 'topRight'});
        });
      });
  }

  open(content) {
    this.modal.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  EditMoney2(moneda_filtro: DefinirMoneda) {
    // se extrae informacion de un moneda
    this.datos_moneda = new DefinirMoneda();
    this.datos_moneda = Object.assign({}, moneda_filtro);
  }

  EditMoney(id: number) {
    // se creo filtro en el modelo de monedas para busca informacion
    let filtermoney = this.money.filter((money: DefinirMoneda) => money.id === id);
    this.EditMoney2(filtermoney[0]);
  }
  

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.getTypes_Money();
    console.log(sessionStorage.getItem('user'));
  }

}
