import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DataService } from './../services/data.service';
import { User } from './../modelos/user';
import { Invitado } from './../modelos/invitados';
import { NgForm } from '@angular/forms';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { AuthServiceToken } from './../shared/auth.service';
import { JWTToken } from './../shared/token';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  token: JWTToken;
  moneda:boolean;
  cuentas:boolean;
  categoria:boolean;
  gast_ingres:boolean;
  transaccion:boolean;
  grafico:boolean;
  nombre:string;
  closeResult: string;
  users: User[] = [];
  invitados: Invitado[] = [];
  user: User;
  img: string;
  correo: string;
  pass:string;
  username: string;
  nombre_invitado: string;

  @Input() invitado: Invitado;

  constructor(private router: Router, private authService: AuthServiceToken, private modalService: NgbModal, private dataService: DataService, public iziToast: Ng2IzitoastService) {
    this.moneda = false;
    this.cuentas= false;
    this.categoria=false;
    this.gast_ingres= false;
    this.transaccion= false;
    this.grafico= true;
  } 

  getUsers(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getUsers(this.token).subscribe(data => {
          this.users = data;
          console.log(this.users);
          this.user = this.users.find(x=> x.correo == sessionStorage.getItem('user'));
          this.nombre = this.user.nombre;
          this.img = this.user.img;
        });
      });
  }

  getInvitados(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getInvitadosByUser(this.token,sessionStorage.getItem('user')).subscribe(data => {
          this.invitados = data;
          console.log(this.invitados);
          let invitado = this.invitados.find(x => x.correo == sessionStorage.getItem('invitado'));
          if(invitado != undefined){
            this.nombre_invitado = invitado.nombre;
          }
        });
      });
  }

  eliminarInvitado(nombre, id){
    let confirm2 = confirm("Deseas Eliminar a: " + nombre);
    if(confirm2){
      this.authService.login().subscribe(
        token => {
          this.token = token;
          this.dataService.deleteInvitado(this.token,id).subscribe(data => { 
            setTimeout(() => {
              this.getInvitados();  
            }, 500);
          });
        this.iziToast.success({title: "Exitoso!",
        message: 'Usuario Eliminado!', position: 'topRight'});
        this.modalService.dismissAll();
      });
    }else{
      return;
    }

  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  open2(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', windowClass: 'my-class'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  addInvitado(form: NgForm){
    let invitado = this.invitados.find(x=> x.correo == this.correo);

    if(sessionStorage.getItem('user') == this.correo){
      this.iziToast.error({title: "Error",
      message: 'No puede ser tu mismo correo!', position: 'topRight'});
    }
    else if(invitado != undefined){
      this.iziToast.error({title: "Error",
      message: 'El usuario ya existe!', position: 'topRight'});
    }else{
      if (this.invitado === undefined) {
        this.invitado = new Invitado();
      }
      this.invitado.correo = this.correo;
      this.invitado.contra = this.pass;
      this.invitado.nombre = this.username;
      this.invitado.correo_admin = sessionStorage.getItem('user');

      this.authService.login().subscribe(
        token => {
          this.token = token;
          this.dataService.addInvitado(this.token,this.invitado).subscribe(data => {
            form.reset();
            this.iziToast.success({title: "Exitoso!",
            message: 'Usuario agregado!', position: 'topRight'});
            this.getInvitados();
            this.modalService.dismissAll();
          });;
        });
    }
  }

  logOut(){
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  ngOnInit() {
    this.getUsers();
    this.getInvitados();

  }

}
