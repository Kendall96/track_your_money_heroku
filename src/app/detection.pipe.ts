import { Pipe, PipeTransform } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';

@Pipe({
  name: 'detection'
})
export class DetectionPipe implements PipeTransform {

  transform(value: any): any {
    return parseInt(value + value);
  }

}
