import { Component, OnInit } from '@angular/core';
import { DataService } from './../services/data.service';
import { GastosIngresos } from '../modelos/gastos-ingresos';
import { Categotia } from '../modelos/categotia';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthServiceToken } from './../shared/auth.service';
import { JWTToken } from './../shared/token';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Ng2IzitoastService } from 'ng2-izitoast';

@Component({
  selector: 'app-gastos-ingresos',
  templateUrl: './gastos-ingresos.component.html',
  styleUrls: ['./gastos-ingresos.component.scss']
})
export class GastosIngresosComponent implements OnInit {

  categorias: Categotia[] = [];
  gastos_ingregos: GastosIngresos[] = [];
  data_gastos_ingregos = new GastosIngresos;
  datos_gastos_ingregos = new GastosIngresos;
  id: number;
  categoria: number;
  tipo: string;
  descripcion: string;
  presupuesto: bigint;
  email_usuario: string;
  closeResult: string;
  token: JWTToken;

  constructor(private dataService: DataService, private authService: AuthServiceToken, private modal: NgbModal, private router: Router, public iziToast: Ng2IzitoastService) { }

  getCategorias(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getCategorias(this.token, sessionStorage.getItem('user')).subscribe(data => { 
          this.categorias = data;
          console.log(this.categorias);
        });
      });
  }

  getGast_ingre(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getGastos(this.token, sessionStorage.getItem('user')).subscribe(data => { 
          this.gastos_ingregos = data;
          console.log(this.gastos_ingregos);
        });
      });
  }

  addGast_ingre(formulario: NgForm){  
    this.data_gastos_ingregos.id = this.id;
    this.data_gastos_ingregos.categoria = this.categoria;
    this.data_gastos_ingregos.tipo = this.tipo;
    this.data_gastos_ingregos.descripcion = this.descripcion;
    this.data_gastos_ingregos.presupuesto = this.presupuesto;
    this.data_gastos_ingregos.email_usuario = sessionStorage.getItem('user');

    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.addGastos(this.token, this.data_gastos_ingregos).subscribe(data => { 
          formulario.reset();
          this.getGast_ingre();
          this.modal.dismissAll();
          this.iziToast.success({title: "Exito",
          message: 'Se agregó un Ingreso', position: 'topRight'});
        });
      });
  }

  updateGast_ingre(formulario1: NgForm){  
    this.data_gastos_ingregos.id = this.datos_gastos_ingregos.id;
    this.data_gastos_ingregos.categoria = this.datos_gastos_ingregos.categoria;
    this.data_gastos_ingregos.tipo = this.datos_gastos_ingregos.tipo;
    this.data_gastos_ingregos.descripcion = this.datos_gastos_ingregos.descripcion;
    this.data_gastos_ingregos.presupuesto = this.datos_gastos_ingregos.presupuesto;
    this.data_gastos_ingregos.email_usuario = sessionStorage.getItem('user');

    console.log(this.datos_gastos_ingregos.tipo);
    
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.updateGastos(this.token, this.data_gastos_ingregos).subscribe(data => { 
          formulario1.reset();
          this.getGast_ingre();
          this.modal.dismissAll();
          
          this.iziToast.success({title: "Exito",
          message: 'Se modificó un Ingreso', position: 'topRight'});
        });
      });
  }

  deleteGast_ingre(id: number){
    //console.log(id + sessionStorage.getItem('user'));
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.deleteGastos(this.token,id).subscribe(date => {
          this.getGast_ingre();
          
          this.iziToast.success({title: "Exito",
          message: 'Se eliminó un Ingreso', position: 'topRight'});
        });
      });
  }

  open(content) {
    this.modal.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  EditGast_ingre2(gastos_filtro: GastosIngresos) {
    // se extrae informacion de un moneda
    this.datos_gastos_ingregos = new GastosIngresos();
    this.datos_gastos_ingregos = Object.assign({}, gastos_filtro);
  }

  EditGast_ingre(id: number) {
    // se creo filtro en el modelo de monedas para busca informacion
    let filtergastos = this.gastos_ingregos.filter((gast_ingre: GastosIngresos) => gast_ingre.id === id);
    this.EditGast_ingre2(filtergastos[0]);
  }
  

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.getGast_ingre(),
    this.getCategorias()
  }

}
