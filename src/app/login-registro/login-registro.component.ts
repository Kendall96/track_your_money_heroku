import { Component, OnInit, Input } from '@angular/core';
import { DataService } from './../services/data.service';
import { User } from './../modelos/user';
import { NgForm } from '@angular/forms';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { Router, ActivatedRoute } from "@angular/router";
import * as sha1 from 'js-sha512';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedinLoginProvider
} from 'angular-6-social-login';
import { Invitado } from './../modelos/invitados';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AuthServiceToken } from './../shared/auth.service';
import { JWTToken } from './../shared/token';

@Component({
  selector: 'app-login-registro',
  templateUrl: './login-registro.component.html',
  styleUrls: ['./login-registro.component.scss']
})
export class LoginRegistroComponent implements OnInit {

  @Input() invitado: Invitado;
  qr: boolean = true;
  correo: string;
  contra: string;

  token: JWTToken;
  closeResult: string;
  code: number;
  users: User[] = [];
  invitados: Invitado[] = [];
  valid: boolean;
  correo_invitado: string;
  pass_invitado: string;
  pass: string;
  newpass: string;
  existe: boolean = false;

  @Input() user: User;

  CodeQR() {
    this.qr = !this.qr;
    this.contra = null;
    this.code = null;
  }

  verifyUser(form: NgForm) {
    let user = this.users.find(x => x.correo == this.correo);

    let invitado = this.invitados.find(x => x.correo == this.correo && x.contra == this.contra);

    if (invitado != undefined) {
      this.iziToast.error({
        title: "Error",
        message: 'El usuario es invitado!', position: 'topRight'
      });
      form.reset();
      return;
    }

    if (user.id_app == null && (this.contra != null || this.code != null)) {

      if (user == undefined) {
        this.iziToast.error({
          title: "Error",
          message: 'El usuario no existe!', position: 'topRight'
        });
      } else if (user.contra == null && this.contra != null) {
        this.iziToast.error({
          title: "Error",
          message: 'Tienes que ingresar el token del QR!', position: 'topRight'
        });
      } else if (user.secret == null && this.code != null) {
        this.iziToast.error({
          title: "Error",
          message: 'Tienes que ingresar tu contraseña!', position: 'topRight'
        });
      } else if (this.contra != null && user.contra == sha1.sha512(this.contra)) {
        sessionStorage.setItem('user', this.correo);
        this.router.navigate(['/home']);
      } else if (this.contra != null && user.contra != sha1.sha512(this.contra)) {
        this.iziToast.error({
          title: "Error",
          message: 'Contraseña Invalida!', position: 'topRight'
        });
      }
      else if (user.secret != null && this.code != null) {
        let codigo = this.code;
        let correo = this.correo;
        this.authService.login().subscribe(
          token => {
            this.token = token;
            console.log(user.secret + "/" + codigo);
            this.dataService.verifyToken(this.token, user.secret, codigo).subscribe(data => {
              console.log(data);
              this.valid = data.valid;
              if (this.valid) {
                sessionStorage.setItem('user', correo);
                this.router.navigate(['/home']);
              } else {
                this.iziToast.error({
                  title: "Error",
                  message: 'Código Invalido!', position: 'topRight'
                });
              }
            });
          });
      }
      form.reset();
    } else {
      this.iziToast.error({
        title: "Error",
        message: 'Por Favor Inicia Sesion con los metodos Correctos!', position: 'topRight'
      });
    }
  }

  getUsers() {
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getUsers(this.token).subscribe(data => {
          this.users = data;
          console.log(this.users);
        });
      });
  }

  getInvitados() {
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getInvitados(this.token).subscribe(data => {
          this.invitados = data;
          console.log(this.invitados);
        });
      });
  }

  checkInvitado() {
    let invitado = this.invitados.find(x => x.correo == this.correo_invitado && (x.contra == this.pass_invitado || x.pass == this.pass_invitado));

    if (invitado != undefined) {
      if(invitado.pass != null && invitado.pass == this.pass_invitado){
        sessionStorage.setItem('invitado', invitado.correo);
        sessionStorage.setItem('user', invitado.correo_admin);
        this.modalService.dismissAll();
        this.router.navigate(['/home']);
      }else if(invitado.pass == null && invitado.contra == this.pass_invitado){
        this.existe = true;
        this.iziToast.success({
          title: "Codigo Correcto",
          message: 'Define una nueva contraseña!', position: 'topRight'
        });
      }else{
        this.existe = false;
        this.iziToast.error({
          title: "Error",
          message: 'Ingresa tu contraseña!', position: 'topRight'
        });
      }
    } else {
      this.existe = false;
      this.iziToast.error({
        title: "Error",
        message: 'Codigo o Contraseña Incorrecto!', position: 'topRight'
      });

    }

  }

  changePass(form: NgForm) {
    if (this.pass != this.newpass) {
      this.iziToast.error({
        title: "Error",
        message: 'Las contraseñas no coinciden!', position: 'topRight'
      });
    } else {
      let invitado = this.invitados.find(x => x.correo == this.correo_invitado);
      if (invitado != undefined) {
        if (this.invitado == undefined) {
          this.invitado = new Invitado;
        }
        this.invitado.id = invitado.id;
        this.invitado.pass = this.pass;
        this.authService.login().subscribe(
          token => {
            this.token = token;
            this.dataService.updateInvitado(this.token, this.invitado).subscribe(data => {
              console.log(this.invitados);
              sessionStorage.setItem('invitado', invitado.correo);
              sessionStorage.setItem('user', invitado.correo_admin);
              this.modalService.dismissAll();
              form.reset();
              this.router.navigate(['/home']);
            });
          });
      }
    }
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  constructor(private dataService: DataService, private authService: AuthServiceToken, private modalService: NgbModal, private router: Router, public iziToast: Ng2IzitoastService, private socialAuthService: AuthService) { }

  socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        let user = this.users.find(x => x.correo == userData.email);
        console.log(socialPlatform + " sign in data : ", userData);
        if (user == undefined) {
          if (this.user === undefined) {
            this.user = new User();
          }
          this.user.correo = userData.email;
          this.user.nombre = userData.name;
          this.user.id_app = userData.id;
          this.user.img = userData.image;


          this.authService.login().subscribe(
            token => {
              this.token = token;
              this.dataService.addUser(this.token, this.user).subscribe(data => {
                sessionStorage.setItem('user', userData.email);
                this.router.navigate(['/home']);
              });
            });

        } else {
          sessionStorage.setItem('user', user.correo);
          this.router.navigate(['/home']);
        }

      }
    );
  }

  ngOnInit() {
    this.getUsers();
    this.getInvitados();
  }

}
