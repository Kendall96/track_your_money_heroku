import { Component, OnInit } from '@angular/core';
import { DataService } from './../services/data.service';
import { Categotia } from '../modelos/categotia';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthServiceToken } from './../shared/auth.service';
import { JWTToken } from './../shared/token';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Ng2IzitoastService } from 'ng2-izitoast';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit {

  categorias: Categotia[] = [];
  data_categorias = new Categotia;
  datos_categorias = new Categotia;
  id: number;
  nombre_catego: string;
  catego_padre: number;
  email_usuario: string;
  closeResult: string;
  defaultCatego1: string; defaultCatego2: string; defaultCatego3: string; defaultCatego4: string;
  respuesta: boolean;
  catego_superior: string;
  tipo_catego: number;
  descripcion: string;
  token: JWTToken;

  constructor(private dataService: DataService, private authService: AuthServiceToken, private modal: NgbModal, private router: Router, public iziToast: Ng2IzitoastService) {
    this.defaultCatego1 = "Canasta basica"; this.defaultCatego2 = "Salario"; this.defaultCatego3 = "Traslado"; this.defaultCatego4 = "Vivienda";
    this.catego_superior = "Categoria Superior";
  }

  verificarCatego(catego) :boolean {
    if((catego == 'Traslado')){
      this.respuesta = false;
      return false;
    }else{
      this.respuesta = true;
      return true;
    }
  }

  getCategorias(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getCategorias(this.token ,sessionStorage.getItem('user')).subscribe(data => { 
          this.categorias = data;
          console.log(this.categorias);
        });
      });
  }

  addCategoria(formulario: NgForm){  
    if(!this.catego_padre){
      this.catego_padre = 0;
    }

    this.data_categorias.id = this.id;
    this.data_categorias.nombre_catego = this.nombre_catego;
    this.data_categorias.catego_padre = this.catego_padre;
    this.data_categorias.email_usuario = sessionStorage.getItem('user');
    this.data_categorias.tipo_catego = this.tipo_catego;

    console.log(this.nombre_catego);

    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.addCategoria(this.token, this.data_categorias).subscribe(data => { 
          formulario.reset();
          this.getCategorias();
          this.iziToast.success({title: "Exito",
          message: 'Se agregó una categoria', position: 'topRight'});
          this.modal.dismissAll();
        });
      });
  }

  updateCategoria(formulario1: NgForm){  
    if(this.datos_categorias.catego_padre != this.datos_categorias.id){
      if(!this.datos_categorias.catego_padre){
        this.datos_categorias.catego_padre = 0;
      }
  
      this.data_categorias.id = this.datos_categorias.id;
      this.data_categorias.nombre_catego = this.datos_categorias.nombre_catego;
      this.data_categorias.catego_padre = this.datos_categorias.catego_padre;
      this.data_categorias.email_usuario = sessionStorage.getItem('user');
      this.data_categorias.tipo_catego = this.datos_categorias.tipo_catego;
      
      this.authService.login().subscribe(
        token => {
          this.token = token;
          this.dataService.updateCategoria(this.token, this.datos_categorias).subscribe(data => { 
            formulario1.reset();
            this.getCategorias();
            this.modal.dismissAll();
            this.iziToast.success({title: "Exito",
            message: 'Se modificó una categoria', position: 'topRight'});
          });
        });
    }else{
      this.iziToast.warning({title: "Advertencia",
        message: 'No pude ser la misma categoria', position: 'topRight'});
    }
    
  }

  //filtro para obtener subcategorias
  delete2(filtro: Categotia){
    this.datos_categorias = new Categotia();
    this.datos_categorias = Object.assign({}, filtro);

    console.log(this.datos_categorias);
  }
  
  deleteCatego(id: number){
    this.categorias;
    let filtersubcatego = this.categorias.filter((catego: Categotia) => catego.catego_padre == id);
    this.delete2(filtersubcatego[0]);

    if(this.datos_categorias.catego_padre){
      this.iziToast.warning({title: "Advertencia",
        message: 'Tiene subcategorias asignadas.', position: 'topRight'});
    }else{
      this.authService.login().subscribe(
        token => {
          this.token = token;
          this.dataService.deleteCategoria(this.token, id).subscribe(date => {
            this.getCategorias();
            this.iziToast.success({title: "Exito",
            message: 'Se eliminó una categoria', position: 'topRight'});
          });
        });
    }
  }

  open(content) {
    this.modal.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  editCatego2(cuenta_filtro: Categotia) {
    // se extrae informacion de cuenta para editar
    this.datos_categorias = new Categotia();
    this.datos_categorias = Object.assign({}, cuenta_filtro);
  }

  editCatego(id: number) {
    // se creo filtro en el modelo de cuentas para busca informacion
    let filtercatego = this.categorias.filter((catego: Categotia) => catego.id === id);
    this.editCatego2(filtercatego[0]);
  }
  

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.getCategorias()
  }

}
