import { Component, OnInit, PipeTransform, ViewChild } from '@angular/core';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { DataService } from './../services/data.service';
import { CuentasUsers } from '../modelos/cuentas-users';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { DefinirMoneda } from '../modelos/definir-moneda';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AuthServiceToken } from './../shared/auth.service';
import { JWTToken } from './../shared/token';
import { Transaccion } from '../modelos/transaccion';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { FormControl } from '@angular/forms';
import { Categotia } from '../modelos/categotia';

@Component({
  selector: 'app-graficos',
  templateUrl: './graficos.component.html',
  styleUrls: ['./graficos.component.scss']
})
export class GraficosComponent implements OnInit {
  cuentas: CuentasUsers[] = [];
  money: DefinirMoneda[] = [];
  categorias_info: Categotia[] = [];
  catego_padre: number;
  closeResult: string;
  transacciones: Transaccion[] = [];
  transaccionesGastos: Transaccion[] = [];
  transaccionesIngresos: Transaccion[] = [];
  token: JWTToken;
  final: Date;
  inicial: Date;
  ingresos: number = 0;
  gastos: number = 0;
  filter: string = '';
  cantid_catego: number = 0;
  total_catego: number[] = [];
  barChartData_catego: string[] = [];
  barChartLabels_catego: string[] = [];
  ultimo_mes: Date;
  ultimo_ano: Date;
  ano: number = 0;
  mes: number = 0;
  t: any[] = [];
  nombre: string;

  constructor(private dataService: DataService, public iziToast: Ng2IzitoastService, private authService: AuthServiceToken, private modal: NgbModal, private router: Router) {
  }

  getCuentas() {
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getCuentas(this.token, sessionStorage.getItem('user')).subscribe(data => {
          this.cuentas = data;
          console.log(this.cuentas);
        });
      });
  }

  limpiarFechasano(){
    this.mes = 0;
    this.inicial = undefined;
    this.final = undefined;
    this.ultimo_ano = null;
    this.ultimo_mes = null;
  }

  limpiarFechasmes(){
    this.ano = 0;
    this.inicial = undefined;
    this.final = undefined;
    this.ultimo_ano = null;
    this.ultimo_mes = null;
  }

  getTransacion() {
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getTransaciones(this.token, sessionStorage.getItem('user')).subscribe(data => {
          this.transacciones = data;
          console.log("transacciones 12");
          console.log(this.transacciones)
        });
      });
  }

  ultimo_ms(){
    this.ultimo_mes = new Date('2019-10-08 05:32:00.000 +00:00');
    this.ultimo_mes = new Date(this.ultimo_mes.getFullYear(), this.ultimo_mes.getMonth(), this.ultimo_mes.getDay()-30);
    console.log(this.ultimo_mes);
    this.inicial = undefined;
    this.ultimo_ano = null;
    this.final = undefined;
    this.mes = 0;
    this.ano = 0;
  }

  ultimo_a(){
    this.ultimo_ano = new Date();
    this.ultimo_ano = new Date(this.ultimo_ano.getFullYear(), this.ultimo_ano.getMonth(), this.ultimo_ano.getDay() - 365);
    this.inicial = undefined;
    this.final = undefined;
    this.ultimo_mes = null;
    this.mes = 0;
    this.ano = 0;
  }

  getTypes_Money() {
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getMoney(this.token, sessionStorage.getItem('user')).subscribe(data => {
          this.money = data;
          console.log(this.money);
        });
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  open(content) {
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title', windowClass: 'app-modal-window', size: 'xl' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  getCategorias(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getCategorias(this.token ,sessionStorage.getItem('user')).subscribe(data => { 
          this.categorias_info = data;
          console.log(this.categorias_info);
          console.log("categorias");
        });
      });
  }

  //graficos
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = [];//'', '', '2008', '2009', '2010', '2011', '2012'
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [{data: [0], label: 'Reporte'}]; //data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'

  @ViewChild('trans', {static: false}) content: any;

  public chartClicked(e: any): void {
    
    if (e.active.length > 0){
      let label = e.active[e.active[0]._chart.tooltip._model.dataPoints[0].datasetIndex].$datalabels.$context.dataset.label;
      this.nombre = label.split('_')[0];
      for (let index = 0; index < this.categorias_info.length; index++) {
        const element = this.categorias_info[index];
        if(label.split('_')[1] == element.id){
          this.t = this.transacciones.filter((catego: Transaccion) => catego.categoria_transaccion == element.id);
          this.open(this.content);
        } 
      }
      console.log(e.active[0]._datasetIndex)
  }
    
  }

  getTransacionMonto() {
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getTransaciones(this.token, sessionStorage.getItem('user')).subscribe(data => {
          this.transacciones = data;
          this.barChartData = [];
          for (let index = 0; index < this.categorias_info.length; index++) {
            const element = this.categorias_info[index];
            let t = this.transacciones.filter((catego: Transaccion) => catego.categoria_transaccion == element.id);
            console.log(t.length)
            this.barChartData[index] = ({data: [t.length], label: element.nombre_catego + "_" + element.id});
          }   
        });
      });
  }

  ngOnInit() {
    this.getCuentas();
    this.getTypes_Money();
    this.getTransacion();
    this.getCategorias();
    this.getTransacionMonto();
  }

}
