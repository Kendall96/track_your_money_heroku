import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { User } from '../shared/users';
import { JWTToken } from './token';

@Injectable()
export class AuthServiceToken {
  private authUrl = 'http://apikykbank.herokuapp.com/login';
  private user = new User('prueba', '123');

  constructor(private http: HttpClient) {}

  public login(): Observable<JWTToken> {
    return this.http
      .post<JWTToken>(`${this.authUrl}`, {
        login: this.user.user,
        password: this.user.password,
      })
      .catch(this.handleError);
    }
      private handleError(err: HttpErrorResponse) {
        // tslint:disable-next-line:no-console
        console.log(err.message);
        return Observable.throw(err.message);
      }
    }
