export class JWTToken {
    success: boolean;
    data: {
        token: string;
    };
}
