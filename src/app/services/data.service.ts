import { Injectable } from '@angular/core'; 
import {
  HttpClient,
  HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { QR } from './../modelos/qr';
import { Token } from './../modelos/token';
import { User } from './../modelos/user';
import { DefinirMoneda } from '../modelos/definir-moneda';
import { Invitado } from './../modelos/invitados';
import { CuentasUsers } from './../modelos/cuentas-users';
import { Categotia } from './../modelos/categotia';
import { GastosIngresos } from './../modelos/gastos-ingresos';
import { Transaccion } from './../modelos/transaccion';

import { JWTToken } from './../shared/token';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  
  getQR(): Observable<QR>{
    return this.http.get<QR>('http://apikykbank.herokuapp.com/qr');
  }

  verifyToken(jwttoken: JWTToken,secret, token): Observable<Token>{
    return this.http.get<Token>('http://apikykbank.herokuapp.com/verify/' + secret + '/' + token, {
      headers: { token: jwttoken.data.token }});
  }

  addUser(jwttoken: JWTToken, data: User): Observable<User>{
    return this.http.post<User>('http://apikykbank.herokuapp.com/user',data, {
      headers: { token: jwttoken.data.token }});
  }

  getUsers(jwttoken: JWTToken): Observable<User[]>{
    return this.http.get<User[]>('http://apikykbank.herokuapp.com/user', {
      headers: { token: jwttoken.data.token }});
  }

  addInvitado(jwttoken: JWTToken, data: Invitado): Observable<Invitado>{
    return this.http.post<Invitado>('http://apikykbank.herokuapp.com/invitado',data, {
      headers: { token: jwttoken.data.token }});
  }

  updateInvitado(jwttoken: JWTToken, data: Invitado): Observable<Invitado>{
    return this.http.put<Invitado>('http://apikykbank.herokuapp.com/invitado',data, {
      headers: { token: jwttoken.data.token }});
  }

  getInvitadosByUser(jwttoken: JWTToken, correo_admin): Observable<Invitado[]>{
    return this.http.get<Invitado[]>('http://apikykbank.herokuapp.com/invitado/' + correo_admin, {
      headers: { token: jwttoken.data.token }});
  }

  getInvitados(jwttoken: JWTToken, ): Observable<Invitado[]>{
    return this.http.get<Invitado[]>('http://apikykbank.herokuapp.com/invitado', {
      headers: { token: jwttoken.data.token }});
  }

  deleteInvitado(jwttoken: JWTToken, id): Observable<Invitado[]>{
    return this.http.delete<Invitado[]>('http://apikykbank.herokuapp.com/invitado/' + id, {
      headers: { token: jwttoken.data.token }});
  }

  google(jwttoken: JWTToken){
    return this.http.get('http://apikykbank.herokuapp.com/google', {
      headers: { token: jwttoken.data.token }});
  }

  getMoney(jwttoken: JWTToken, email_usuario): Observable<DefinirMoneda[]>{
    return this.http.get<DefinirMoneda[]>('http://apikykbank.herokuapp.com/typesmoney/' + email_usuario, {
      headers: { token: jwttoken.data.token }});
  }

  addMoney(jwttoken: JWTToken, data: DefinirMoneda): Observable<DefinirMoneda>{
    console.log(data);
    return this.http.post<DefinirMoneda>('http://apikykbank.herokuapp.com/typesmoney', data, {
      headers: { token: jwttoken.data.token }});
  }

  updateMoney(jwttoken: JWTToken, data: DefinirMoneda): Observable<DefinirMoneda>{
    return this.http.put<DefinirMoneda>('http://apikykbank.herokuapp.com/updatemoney', data, {
      headers: { token: jwttoken.data.token }});
  }

  deleteMoneys(jwttoken: JWTToken, id): Observable<DefinirMoneda[]>{
    return this.http.delete<DefinirMoneda[]>('http://apikykbank.herokuapp.com/deletemoney/'+ id, {
      headers: { token: jwttoken.data.token }});
  }

  getCuentas(jwttoken: JWTToken, email_usuario): Observable<CuentasUsers[]>{
    return this.http.get<CuentasUsers[]>('http://apikykbank.herokuapp.com/cuentasusers/' + email_usuario, {
      headers: { token: jwttoken.data.token }});
  }

  addCuentas(jwttoken: JWTToken, data: CuentasUsers): Observable<CuentasUsers>{
    return this.http.post<CuentasUsers>('http://apikykbank.herokuapp.com/cuentasusers', data, {
      headers: { token: jwttoken.data.token }});
  }

  updateCuentas(jwttoken: JWTToken, data: CuentasUsers): Observable<CuentasUsers>{
    return this.http.put<CuentasUsers>('http://apikykbank.herokuapp.com/updatecuentas', data, {
      headers: { token: jwttoken.data.token }});
  }

  deleteCuentas(jwttoken: JWTToken, id): Observable<CuentasUsers[]>{
    return this.http.delete<CuentasUsers[]>('http://apikykbank.herokuapp.com/deletecuentas/'+ id, {
      headers: { token: jwttoken.data.token }});
  }

  //Rutas Categorias
  getCategorias(jwttoken: JWTToken, email_usuario): Observable<Categotia[]>{
    console.log(email_usuario + " esta en servicio.");
    return this.http.get<Categotia[]>('http://apikykbank.herokuapp.com/categoria/' + email_usuario, {
      headers: { token: jwttoken.data.token }});
  }

  addCategoria(jwttoken: JWTToken, data: Categotia): Observable<Categotia>{
    return this.http.post<Categotia>('http://apikykbank.herokuapp.com/categoria', data, {
      headers: { token: jwttoken.data.token }});
  }

  updateCategoria(jwttoken: JWTToken, data: Categotia): Observable<Categotia>{
    return this.http.put<Categotia>('http://apikykbank.herokuapp.com/updatecategoria', data, {
      headers: { token: jwttoken.data.token }});
  }

  deleteCategoria(jwttoken: JWTToken, id): Observable<Categotia[]>{
    return this.http.delete<Categotia[]>('http://apikykbank.herokuapp.com/deletecategoria/'+ id, {
      headers: { token: jwttoken.data.token }});
  }

  //Rutas Gastos e Ingresos
  getGastos(jwttoken: JWTToken, email_usuario): Observable<GastosIngresos[]>{
    console.log(email_usuario + " esta en servicio.");
    return this.http.get<GastosIngresos[]>('http://apikykbank.herokuapp.com/gastingre/' + email_usuario, {
      headers: { token: jwttoken.data.token }});
  }

  addGastos(jwttoken: JWTToken, data: GastosIngresos): Observable<GastosIngresos>{
    return this.http.post<GastosIngresos>('http://apikykbank.herokuapp.com/gastingre', data, {
      headers: { token: jwttoken.data.token }});
  }

  updateGastos(jwttoken: JWTToken, data: GastosIngresos): Observable<GastosIngresos>{
    return this.http.put<GastosIngresos>('http://apikykbank.herokuapp.com/updategastingre', data, {
      headers: { token: jwttoken.data.token }});
  }

  deleteGastos(jwttoken: JWTToken, id): Observable<GastosIngresos[]>{
    return this.http.delete<GastosIngresos[]>('http://apikykbank.herokuapp.com/deletegastingre/'+ id, {
      headers: { token: jwttoken.data.token }});
  }

  //Rutas Transacciones
  getTransaciones(jwttoken: JWTToken, email_usuario): Observable<Transaccion[]>{
    console.log(email_usuario + " esta en servicio.");
    return this.http.get<Transaccion[]>('http://apikykbank.herokuapp.com/transaction/' + email_usuario, {
      headers: { token: jwttoken.data.token }});
  }

  addTransacciones(jwttoken: JWTToken, data: Transaccion): Observable<Transaccion>{
    return this.http.post<Transaccion>('http://apikykbank.herokuapp.com/transaction', data, {
      headers: { token: jwttoken.data.token }});
  }

  updateTransacciones(jwttoken: JWTToken, data: Transaccion): Observable<Transaccion>{
    return this.http.put<Transaccion>('http://apikykbank.herokuapp.com/updatetransaction', data, {
      headers: { token: jwttoken.data.token }});
  }

  deleteTransacciones(jwttoken: JWTToken, id): Observable<Transaccion[]>{
    return this.http.delete<Transaccion[]>('http://apikykbank.herokuapp.com/deletetransaction/'+ id, {
      headers: { token: jwttoken.data.token }});
  }

}