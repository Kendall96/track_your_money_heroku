import { Component, OnInit } from '@angular/core';
import { DataService } from './../services/data.service';
import { CuentasUsers } from '../modelos/cuentas-users';
import { DefinirMoneda } from '../modelos/definir-moneda';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthServiceToken } from './../shared/auth.service';
import { JWTToken } from './../shared/token';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Ng2IzitoastService } from 'ng2-izitoast';

@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.component.html',
  styleUrls: ['./cuentas.component.scss']
})
export class CuentasComponent implements OnInit {

  money: DefinirMoneda[] = [];
  cuentas: CuentasUsers[] = [];
  data_cuentas = new CuentasUsers;
  datos_cuentas = new CuentasUsers;
  id: number;
  descripcion_moneda: number;
  nombre_cuenta: string;
  descripcion: string;
  saldo_inicial: number;
  email_usuario: string;
  closeResult: string;
  token: JWTToken;

  constructor(private dataService: DataService, public iziToast: Ng2IzitoastService, private authService: AuthServiceToken, private modal: NgbModal, private router: Router) { }

  getTypes_Money(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getMoney(this.token, sessionStorage.getItem('user')).subscribe(data => { 
          this.money = data;
          console.log(this.money);
        });
      });
  }

  getCuentas(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getCuentas(this.token, sessionStorage.getItem('user')).subscribe(data => { 
          this.cuentas = data;
          console.log(this.cuentas);
        });
      });
  }

  addCuenta(formulario: NgForm){  
    this.data_cuentas.id = this.id;
    this.data_cuentas.descripcion_moneda = this.descripcion_moneda;
    this.data_cuentas.nombre_cuenta = this.nombre_cuenta;
    this.data_cuentas.descripcion = this.descripcion;
    this.data_cuentas.saldo_inicial = this.saldo_inicial;
    this.data_cuentas.email_usuario = sessionStorage.getItem('user');

    console.log(this.descripcion_moneda);
    console.log(this.nombre_cuenta);

    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.addCuentas(this.token, this.data_cuentas).subscribe(data => { 
          formulario.reset();
          this.getCuentas();
          this.modal.dismissAll();

          this.iziToast.success({title: "Exito",
          message: 'Se creó una cuenta', position: 'topRight'});
        });
      });
  }

  updateCuenta(formulario1: NgForm){  
    this.data_cuentas.id = this.datos_cuentas.id;
    this.data_cuentas.descripcion_moneda = this.descripcion_moneda;
    this.data_cuentas.nombre_cuenta = this.datos_cuentas.nombre_cuenta;
    this.data_cuentas.descripcion = this.datos_cuentas.descripcion;
    this.data_cuentas.saldo_inicial = this.datos_cuentas.saldo_inicial;
    this.data_cuentas.email_usuario = sessionStorage.getItem('user');
    
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.updateCuentas(this.token, this.data_cuentas).subscribe(data => { 
          formulario1.reset();
          this.getCuentas();
          this.modal.dismissAll();
          this.iziToast.success({title: "Exito",
          message: 'Se modificó una cuenta', position: 'topRight'});
        });
      });
  }
  
  deleteAccount(id: number){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.deleteCuentas(this.token, id).subscribe(date => {
          setTimeout(() => {
            this.getCuentas();
            this.iziToast.success({title: "Exito",
            message: 'Se eliminó una cuenta', position: 'topRight'});
          }, 50);
        });
      });
  }

  open(content) {
    this.modal.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  editAccount2(cuenta_filtro: CuentasUsers) {
    // se extrae informacion de cuenta para editar
    this.datos_cuentas = new CuentasUsers();
    this.datos_cuentas = Object.assign({}, cuenta_filtro);
  }

  editAccount(id: number) {
    // se creo filtro en el modelo de cuentas para busca informacion
    let filteraccount = this.cuentas.filter((account: CuentasUsers) => account.id === id);
    this.editAccount2(filteraccount[0]);
  }
  

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }


  ngOnInit() {
    this.getCuentas(),
    this.getTypes_Money()
  }

}
