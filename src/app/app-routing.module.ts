import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginRegistroComponent } from './login-registro/login-registro.component';
import { ErrorComponent } from './error/error.component';
import { RegistroComponent } from './registro/registro.component';
import { HomeComponent } from './home/home.component';
import { DefinirMonedaComponent } from './definir-moneda/definir-moneda.component';
import { CuentasComponent } from './cuentas/cuentas.component';
import { GastosIngresosComponent } from './gastos-ingresos/gastos-ingresos.component';
import { TransaccionesComponent } from './transacciones/transacciones.component';
import { GraficosComponent } from './graficos/graficos.component';
import { AdminGuard } from './guards/admin.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [  
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginRegistroComponent},
  { path: 'registro' , component: RegistroComponent},
  { path: 'home', component: HomeComponent , canActivate: [AdminGuard]}, //,
  { path: 'moneda', component: DefinirMonedaComponent , canActivate: [AdminGuard]},
  { path: 'cuentas', component: CuentasComponent , canActivate: [AdminGuard]},
  { path: 'gastos_ingresos', component: GastosIngresosComponent , canActivate: [AdminGuard]},
  { path: 'transacion', component: TransaccionesComponent , canActivate: [AdminGuard]},
  { path: 'graficos', component: GraficosComponent , canActivate: [AdminGuard]},
  { path: '**', component: ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
