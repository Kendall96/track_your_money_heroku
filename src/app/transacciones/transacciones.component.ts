import { Component, OnInit, Input } from '@angular/core';
import { DataService } from './../services/data.service';
import { Transaccion } from '../modelos/transaccion';
import { Transaccion2 } from '../modelos/transaccion2';
import { Categotia } from '../modelos/categotia';
import { CuentasUsers } from '../modelos/cuentas-users';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AuthServiceToken } from './../shared/auth.service';
import { JWTToken } from './../shared/token';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { DefinirMoneda } from '../modelos/definir-moneda';

@Component({
  selector: 'app-transacciones',
  templateUrl: './transacciones.component.html',
  styleUrls: ['./transacciones.component.scss'],
  providers: [DatePipe]
})
export class TransaccionesComponent implements OnInit {
  
  @Input() datacuenta = new CuentasUsers;
  money: DefinirMoneda[] = [];
  categorias: Categotia[] = [];
  cuentas: CuentasUsers[] = [];
  transacciones: Transaccion[] = [];
  data_cuentas = new CuentasUsers;
  datas_transactions = new Transaccion2;
  data_transaction = new Transaccion;
  datos_transaccion = new Transaccion;
  id: number;
  tipo_transaccion: number;
  tipo_transaccion2: number;
  fecha: Date;
  cuenta_transaccion: number;
  categoria_transaccion: number;
  categoria_transaccion2: number;
  detalle: string;
  monto: number;
  email_usuario: string;
  closeResult: string;
  fecha2 = new Date();
  fecha3: string;
  token: JWTToken;
  cuenta_destino: number;
  montoActual: number;
  tipo_trans: boolean = false;
  doble_trans: boolean = false;
  operacion: number = 0;
  operacion2: number = 0;

  constructor(private dataService: DataService, private authService: AuthServiceToken, private modal: NgbModal, private router: Router, public iziToast: Ng2IzitoastService, private datePipe: DatePipe) { 
    this.fecha3 = this.datePipe.transform(this.fecha2, 'yyyy-MM-dd h:mm');
  }

  getTypes_Money(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getMoney(this.token, sessionStorage.getItem('user')).subscribe(data => { 
          this.money = data;
          console.log(this.money);
        });
      });
  }

  getCategorias(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getCategorias(this.token, sessionStorage.getItem('user')).subscribe(data => { 
          this.categorias = data;
          console.log(this.categorias);
        });
      });
  }
 
  verificarCuenta(){
    console.log(this.cuenta_transaccion);

    let monto = this.cuentas.find(x => x.id == this.cuenta_transaccion);

    if(monto != undefined){
      this.montoActual = monto.saldo_inicial;
    }
  }

  verificarTraslado(){
    let traslado = this.categorias.find(x => x.id == this.categoria_transaccion);
    if(traslado.nombre_catego == 'Traslado'){
      this.tipo_trans = true;
    }else{
      this.tipo_trans = false;
    }
  }

  getCuentas(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getCuentas(this.token, sessionStorage.getItem('user')).subscribe(data => { 
          this.cuentas = data;
          console.log(this.cuentas);
        });
    });
  }
  
  getTransacion(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getTransaciones(this.token, sessionStorage.getItem('user')).subscribe(data => { 
          this.transacciones = data;
          console.log(this.transacciones);
        });
      });
  }

  //Falta modificar que se le reste a la cuenta el monto o se le sume deacuerdo al tipo de transaccion
  // se necesita crear filtro en las cuentas para obtener la informacion y asi poder actulizar la cuenta
  addTransacion(formulario: NgForm){  
    let nombre_tran = '';
    let nombre_tran2 = '';
    let cuenta = this.cuentas.find(x => x.id == this.cuenta_transaccion);
    let cuenta_destino = this.cuentas.find(x => x.id == this.cuenta_destino);

    if(this.tipo_transaccion == 0 && this.monto > this.montoActual){
      this.iziToast.warning({title: "Advertencia",
      message: 'El monto solicitado no puede ser mayor al Actual!', position: 'topRight'});
    }else{
      let traslado = this.categorias.find(x => x.id == this.categoria_transaccion);
      let cambio1 = this.money.find(x => x.id == cuenta.descripcion_moneda);
      let cambio2;
      if(cuenta_destino != undefined){
        cambio2 = this.money.find(x => x.id == cuenta_destino.descripcion_moneda);
      }
      //esta es para cuando la transacion es ingreso
      if(this.tipo_transaccion == 1 && traslado.nombre_catego != 'Traslado'){
        this.updateCuenta(cuenta.id, parseInt(cuenta.saldo_inicial.toString()) + this.monto); 

        nombre_tran = 'Ingreso';
        nombre_tran2 = 'Gasto';
      }
      //esta es para cuando la trasaccion es gasto
      else if(this.tipo_transaccion == 0 && traslado.nombre_catego != 'Traslado'){
        this.updateCuenta(cuenta.id, this.montoActual-this.monto); 

        nombre_tran = 'Gasto';
        nombre_tran2 = 'Ingreso';
      }
      //cuando genera un translado tipo gasto
      else if(this.tipo_transaccion == 0 && traslado.nombre_catego == 'Traslado'){
        this.doble_trans = true;
        this.tipo_transaccion = 0;
        this.tipo_transaccion2 = 1;
        this.categoria_transaccion2 = 3;
        nombre_tran = 'Gasto';
        nombre_tran2 = 'Ingreso';

        if(cuenta.id == cuenta_destino.id){
          this.iziToast.warning({title: "Advertencia",
          message: 'Selecciona una cuenta distinta a la de origen!', position: 'topRight'});
          return;
        }
        else if(cambio1.nombre_moneda.toLocaleLowerCase().trim() == "dolar"){
          console.log(this.monto * parseInt(cambio2.tasa.toString()));
          this.operacion2 = this.monto * parseInt(cambio2.tasa.toString());
          this.operacion = parseInt(cuenta_destino.saldo_inicial.toString()) + (this.monto * parseInt(cambio2.tasa.toString()));

          this.updateCuenta(cuenta.id, parseInt(cuenta.saldo_inicial.toString()) - this.monto);
          this.updateCuenta2(cuenta_destino.id, this.operacion);
         }else{
          console.log(this.monto / parseInt(cambio2.tasa.toString()));
          this.operacion2 = this.monto / parseInt(cambio2.tasa.toString());
          this.operacion = parseInt(cuenta_destino.saldo_inicial.toString()) + (this.monto / parseInt(cambio2.tasa.toString()));
          
          this.updateCuenta2(cuenta_destino.id, this.operacion);
          this.updateCuenta(cuenta.id, parseInt(cuenta.saldo_inicial.toString()) - this.monto);
        }
      }
      //cuando genera un translado tipo ingreso
      else if(this.tipo_transaccion == 1 && traslado.nombre_catego == 'Traslado'){ 
        this.doble_trans = true;
        this.tipo_transaccion = 0;
        this.tipo_transaccion2 = 1;
        this.categoria_transaccion2 = 4;
        nombre_tran = 'Gasto';
        nombre_tran2 = 'Ingreso';

        if(cuenta.id == cuenta_destino.id){
          this.iziToast.warning({title: "Advertencia",
          message: 'Selecciona una cuenta distinta a la de origen!', position: 'topRight'});
          return;
        }
        else if(cambio1.nombre_moneda.toLocaleLowerCase().trim() == "dolar"){
          console.log(this.monto * parseInt(cambio2.tasa.toString()));
          this.operacion2 = this.monto * parseInt(cambio2.tasa.toString());
          this.operacion = parseInt(cuenta_destino.saldo_inicial.toString()) + (this.monto * parseInt(cambio2.tasa.toString()));

          this.updateCuenta(cuenta.id, parseInt(cuenta.saldo_inicial.toString()) - this.monto);
          this.updateCuenta2(cuenta_destino.id, this.operacion);
         }else{
          console.log(this.monto / parseInt(cambio2.tasa.toString()));
          this.operacion2 = this.monto / parseInt(cambio2.tasa.toString());
          this.operacion = parseInt(cuenta_destino.saldo_inicial.toString()) + (this.monto / parseInt(cambio2.tasa.toString()));

          this.updateCuenta2(cuenta_destino.id, this.operacion);
          this.updateCuenta(cuenta.id, parseInt(cuenta.saldo_inicial.toString()) - this.monto);
        }
      }
      //let nombre_tran = '';
      /*if(this.tipo_transaccion == 0){
        nombre_tran = 'Gasto';
      }else{
        nombre_tran = 'Ingreso';
      }

      let nombre_tran2 = '';
      if(nombre_tran == 'Gasto'){
        nombre_tran2 = 'Ingreso';
      }else{
        nombre_tran2 = 'Gasto';
      }*/

      this.data_transaction.tipo_transaccion = this.tipo_transaccion;
      this.data_transaction.fecha = new Date().toString();   //this.fecha3;
      this.data_transaction.cuenta_transaccion = this.cuenta_transaccion;
      this.data_transaction.categoria_transaccion = this.categoria_transaccion;
      this.data_transaction.detalle = this.detalle;
      this.data_transaction.monto = this.monto;
      this.data_transaction.email_usuario = sessionStorage.getItem('user');
      this.data_transaction.nombre_cuenta = cuenta.nombre_cuenta;
      this.data_transaction.nombre_transaccion = nombre_tran;

      this.authService.login().subscribe(
        token => {
          this.token = token;
          this.dataService.addTransacciones(this.token, this.data_transaction).subscribe(data => { 
            formulario.reset();
            this.getTransacion();
            this.modal.dismissAll();
            this.iziToast.success({title: "Exito",
            message: 'Se agregó una transacción', position: 'topRight'});
          });
      });

      if(this.doble_trans == true){

        this.datas_transactions.tipo_transaccion = this.tipo_transaccion2;
        this.datas_transactions.fecha = new Date().toString(); 
        this.datas_transactions.cuenta_transaccion = cuenta_destino.id;
        this.datas_transactions.categoria_transaccion = this.categoria_transaccion2;
        this.datas_transactions.detalle = this.detalle;
        this.datas_transactions.monto =  this.operacion2;
        this.datas_transactions.email_usuario = sessionStorage.getItem('user');
        this.datas_transactions.nombre_cuenta = cuenta_destino.nombre_cuenta;
        this.datas_transactions.nombre_transaccion = nombre_tran2;

        this.authService.login().subscribe(
          token => {
            this.token = token;
            this.dataService.addTransacciones(this.token, this.datas_transactions).subscribe(data => { 
              formulario.reset();
              this.getTransacion();
              this.modal.dismissAll();
              this.iziToast.success({title: "Exito",
              message: 'Se agregó una transacción', position: 'topRight'});
            });
          });


        this.doble_trans = false;
        this.operacion = 0;
      }
    }
  }

  updateTransacion(formulario1: NgForm){  
    this.data_transaction.id = this.datos_transaccion.id;
    this.data_transaction.tipo_transaccion = this.datos_transaccion.tipo_transaccion;
    this.data_transaction.fecha =  this.datos_transaccion.fecha; // new Date().toString();   ya se obtiene automaticamente
    this.data_transaction.cuenta_transaccion = this.datos_transaccion.cuenta_transaccion;
    this.data_transaction.categoria_transaccion = this.datos_transaccion.categoria_transaccion;
    this.data_transaction.detalle = this.datos_transaccion.detalle;
    this.data_transaction.monto = this.datos_transaccion.monto;
    this.data_transaction.email_usuario = sessionStorage.getItem('user');

    console.log(this.data_transaction.detalle);
    
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.updateTransacciones(this.token, this.data_transaction).subscribe(data => { 
          formulario1.reset();
          this.getTransacion();
          this.modal.dismissAll();
          this.iziToast.success({title: "Exito",
          message: 'Se modificó una transacción!', position: 'topRight'});
        });
      });
  }

  deleteTransacion(id: number){
    //console.log(id + sessionStorage.getItem('user'));
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.deleteTransacciones(this.token, id).subscribe(date => {
          this.getTransacion();          
          this.iziToast.success({title: "Exito",
          message: 'Se eliminó una transacción', position: 'topRight'});
        });
      });
  }

  open(content) {
    this.modal.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  EditTransacion2(transact: Transaccion) {
    // se extrae informacion de un moneda
    this.datos_transaccion = new Transaccion();
    this.datos_transaccion = Object.assign({}, transact);
  }

  EditTransacion(id: number) {
    // se creo filtro en el modelo de monedas para busca informacion
    let filtertransactio = this.transacciones.filter((transac: Transaccion) => transac.id === id);
    this.EditTransacion2(filtertransactio[0]);
  }
  
  updateCuenta(id, saldo){  
    this.data_cuentas.id = id;
    this.data_cuentas.saldo_inicial = saldo;
    this.data_cuentas.email_usuario = sessionStorage.getItem('user');

    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.updateCuentas(this.token, this.data_cuentas).subscribe(data => {
          this.getCuentas();
        });
      });
  }

  updateCuenta2(id, saldo){  
    this.datacuenta.id = id;
    this.datacuenta.saldo_inicial = saldo;
    this.datacuenta.email_usuario = sessionStorage.getItem('user');

    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.updateCuentas(this.token, this.datacuenta).subscribe(data => {
          this.getCuentas();
        });
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.getTransacion(),
    this.getCategorias(),
    this.getCuentas(),
    this.getTypes_Money()
  }

}
