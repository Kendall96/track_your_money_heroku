import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginRegistroComponent } from './login-registro/login-registro.component';
import { ErrorComponent } from './error/error.component';
import { RegistroComponent } from './registro/registro.component';
import { DataService } from './services/data.service';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedinLoginProvider,
} from "angular-6-social-login";
import { Ng2IziToastModule, Ng2IzitoastService } from 'ng2-izitoast';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { HomeComponent } from './home/home.component';
import { DefinirMonedaComponent } from './definir-moneda/definir-moneda.component';
import { CuentasComponent } from './cuentas/cuentas.component';
import { GastosIngresosComponent } from './gastos-ingresos/gastos-ingresos.component';
import { TransaccionesComponent } from './transacciones/transacciones.component';
import { GraficosComponent } from './graficos/graficos.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { MoneyTransformPipe } from './money-transform.pipe';
import { CategoriasComponent } from './categorias/categorias.component';
import { ChartsModule } from 'ng2-charts';
import { AuthServiceToken } from './shared/auth.service'; 
import { FiltroPEPipe } from './filtros/filtroPE.pipe';
import { DetectionPipe } from './detection.pipe';
import { TextoPipe } from './filtros/texto.pipe';
import {NgxPaginationModule} from 'ngx-pagination';
import { TotalPipe } from './filtros/total.pipe';
import { FechatotalPipe } from './filtros/fechatotal.pipe';

// Configs 
const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("289917391122-daptnksciuj6b8vk2vouemejkic6uiab.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('325952794978098')
  }
]);

export function provideConfi(){
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginRegistroComponent,
    ErrorComponent,
    RegistroComponent,
    HomeComponent,
    DefinirMonedaComponent,
    CuentasComponent,
    GastosIngresosComponent,
    TransaccionesComponent,
    GraficosComponent,
    MoneyTransformPipe,
    FiltroPEPipe,
    CategoriasComponent,
    DetectionPipe,
    TextoPipe,
    TotalPipe,
    FechatotalPipe
  ],
  imports: [
    NgbModule,
    FormsModule,
    ShowHidePasswordModule,
    NgxPaginationModule,
    BrowserModule,
    ChartsModule,
    Ng2IziToastModule,ReactiveFormsModule,
    SocialLoginModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [DataService,Ng2IzitoastService, AuthServiceToken, {provide: AuthServiceConfig, useFactory: provideConfi}],
  bootstrap: [AppComponent]
})
export class AppModule { }
