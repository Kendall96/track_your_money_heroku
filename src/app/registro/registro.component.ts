import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import * as sha1 from 'js-sha512';
import { DataService } from './../services/data.service';
import { User } from './../modelos/user';
import { NgForm } from '@angular/forms';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { AuthServiceToken } from './../shared/auth.service';
import { JWTToken } from './../shared/token';
import { Invitado } from './../modelos/invitados';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  constructor(private dataService: DataService, private authService: AuthServiceToken, private router: Router, public iziToast: Ng2IzitoastService) { }

  @Input() user: User;
  nombre: string;
  invitados: Invitado[] = [];
  cedula: number;
  correo: string;
  contra: string;
  contra2: string;
  secret: string; 
  qr: boolean = true;
  img: any; 
  users: User[] = [];
  token: JWTToken;

  QR(){
    this.qr = !this.qr;
    if(!this.qr){
      this.getIMG();
    }
  }

  getIMG(){
    this.dataService.getQR().subscribe(data => {
      console.log(data);
      this.img = data.data;
      this.secret = data.secret;
    });
  }

  getInvitados(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getInvitados(this.token).subscribe(data => {
          this.invitados = data;
          console.log(this.invitados);
        });
      });
  }

  addUser(form: NgForm){
    
    let user = this.users.find(x=> x.correo == this.correo || x.cedula == this.cedula);
    let invitado = this.invitados.find(x => x.correo == this.correo);

    if(this.contra != this.contra2){
      this.iziToast.error({title: "Error",
      message: 'Las contraseñas no coinciden!', position: 'topRight'});
    }else if(user == undefined){
      if (this.user === undefined) {
        this.user = new User();
      }
      if(!this.qr){
        this.user.correo = this.correo;
        this.user.cedula = this.cedula;
        this.user.nombre = this.nombre;
        this.user.secret = this.secret;  
      }else{
        this.user.correo = this.correo;
        this.user.cedula = this.cedula;
        this.user.nombre = this.nombre;
        this.user.contra = sha1.sha512(this.contra);
      }

      this.authService.login().subscribe(
        token => {
          this.token = token;
          this.dataService.addUser(this.token, this.user).subscribe(data => {
            form.reset();
            this.router.navigate(['/login']);
          });
        });
    }else if(invitado != undefined){
      this.iziToast.error({title: "Error",
      message: 'Ya es usuario invitado!', position: 'topRight'});
      form.reset();
    }else{
      this.iziToast.error({title: "Error",
      message: 'El usuario ya existe!', position: 'topRight'});
      form.reset();
    }
  }

  getUsers(){
    this.authService.login().subscribe(
      token => {
        this.token = token;
        this.dataService.getUsers(this.token).subscribe(data => {
          this.users = data;
          console.log(this.users);
        });
      });
  }

  ngOnInit() {
    this.getInvitados();
    this.getUsers();
  }

}
