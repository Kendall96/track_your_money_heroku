export class GastosIngresos {
    id: number;
    categoria: number;
    tipo: string;
    descripcion: string;
    presupuesto: bigint;
    email_usuario: string;
}
