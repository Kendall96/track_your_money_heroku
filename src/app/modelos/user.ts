export class User{
    nombre: string;
    cedula: number;
    correo: string;
    contra: string;
    secret: string;
    id_app: string;
    img: string;
    codigo: string;
}