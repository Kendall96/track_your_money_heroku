export class Transaccion {
    id: number;
    tipo_transaccion: number;
    fecha: string;
    cuenta_transaccion: number;
    categoria_transaccion: number;
    detalle: string;
    monto: number;
    email_usuario: string;
    nombre_cuenta: string;
    nombre_transaccion: string;
}
