export class DefinirMoneda {
    id: number;
    nombre_moneda: string;
    simbolo: string;
    descripcion: string;
    tasa: bigint;
    email_usuario: string;
}
