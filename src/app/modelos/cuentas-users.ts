export class CuentasUsers {
    id: number;
    descripcion_moneda: number;
    nombre_cuenta: string;
    descripcion: string;
    saldo_inicial: number;
    email_usuario: string;
}
